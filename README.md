# Developing a Five Year Plan to mitigate climate change assuming a global revolution

## Abstract
We want to find out what climate mitigation policy will look like if every country were to be controlled by the revolutionary proletariat. 

## Introduction
Although there is a lot of research in climate science on climate policy, climate activism in first would countries has been limited in its ability to address the political contradictions that prevent us from acting sufficiently urgently. This project attempts to fill this gap by presenting the capability of anti-imperialism and anti-settler colonialism on tackling these issues. The focus will be on valuing the voice and action plans of people of the global south whose labor fuels the rapid development and destructive practices in the first world. 

## Literature Review
+ Kyoto Protocol - 1992 agreement to reduce 6 greenhouse gasses based on common but differentiated responsibilities where developed countries have the responsbility to reduce more since they are historically responsible for the crisis. 
+ Paris convention - Aims: 
    1.  Holding the increase in the global average temperature to well below 2°C above pre-industrial levels and to pursue efforts to limit the temperature increase to 1.5°C above pre-industrial levels, recognizing that this would significantly reduce the risks and impacts of climate change;

    2. Increasing the ability to adapt to the adverse impacts of climate change and foster climate resilience and low greenhouse gas emissions development, in a manner that does not threaten food production;

    3. Making finance flows consistent with a pathway towards low greenhouse gas emissions and climate-resilient development.


## Methodology 
We first want to do an in-depth examination into the UN reports and the agreements between countries in the reports above, or literature that analyzes these agreements. Doing so will help us identify problem areas with the current liberal approach, and which areas are salvageable. 

Then we want to determine what climate policy makers and scientists have deemed their goal is in their respective countries today. 

The main source for this will be online research, whether that be documents by climate activists, or scholarly articles, or books. Where possible, we also want to consult climate scientists and policy makers individually through email or phone, or directly. 

Then we can decide which policies are consistent with a dictatorship of the proletariat and which are not. If needed, we may have to engage with raw data to fill the gap for the plan, which will take some expertise, so contacting experts will be crucial at this stage. 


## Timeframe
Our goal is to finish our plan by November 3rd 2020, which is 2020 US president election day. The intent here is that there will be a lot of political conversation, and that we can hijack some of the traffic. 

That leaves us with less than 486 days, which is 69 (nice) weeks. 

In the meanwhile, we also plan on making podcast episodes to share our knowledge gained from research. 

